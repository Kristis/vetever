document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems, {height: 600});

	// window.next = function() {
	// 	var el = document.querySelector(".slider");
	// 	var instance = M.Slider.getInstance(el);
	// 	instance.next();
 // 	}

 // 	window.prev = function() {
	// 	var el = document.querySelector(".slider");
	// 	var instance = M.Slider.getInstance(el);
	// 	instance.prev();
 // 	}
});

function next() {
	var el = document.querySelector(".slider");
	var instance = M.Slider.getInstance(el);
	instance.next();
}

function prev() {
	var el = document.querySelector(".slider");
	var instance = M.Slider.getInstance(el);
	instance.prev();
}