var ctx1 = document.getElementById('chart1').getContext('2d');
var chart1 = new Chart(ctx1, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: ['2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'],
        datasets: [{
            label: 'Pagydyti gyvūnai iš viso',
            backgroundColor: '#a5d6a7',
            borderColor: '#80cbc4',
            data: [500, 1500, 2800, 4200, 5700, 7800, 9500, 11300]
        }]
    },

    // Configuration options go here
    options: {}
});

var ctx2 = document.getElementById('chart2').getContext('2d');
var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: ['Katės', 'Šunys', 'Triušiai', 'Žiurkėnai'],
        datasets: [{
            backgroundColor: ['#c0ca33', '#66bb6a', '#00b0ff', '#00bfa5'],
            data: [13800, 10900, 10400, 8200]
        }]
    },

    // Configuration options go here
    options: {
    	cutoutPercentage: 50,
    }
});

var ctx3 = document.getElementById('chart3').getContext('2d');
var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'],
        datasets: [{
            label: 'Savanoriauta valandų',
            backgroundColor: '#7cb342',
            borderColor: '#81d4fa',
            data: [48, 62, 58, 74, 76, 59, 82, 87]
        }]
    },

    // Configuration options go here
    options: {}
});

var ctx4 = document.getElementById('chart4').getContext('2d');
var chart4 = new Chart(ctx4, {
    // The type of chart we want to create
    type: 'radar',

    // The data for our dataset
    data: {
        labels: ['Kruopštumas', 'Atsakingumas', 'Švelnumas', 'Greitis', 'Profesionalumas', 'Žinios'],
        datasets: [{
            label: 'Klientų vertinimas',
            backgroundColor: '#ffc107',
            borderColor: '#4db6ac',
            data: [70, 85, 75, 90, 85, 95]
        }]
    },

    // Configuration options go here
    options: {}
});
 