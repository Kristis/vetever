// var ctx1 = document.getElementById('chart1').getContext('2d');
// var chart1 = new Chart(ctx1, {
//     // The type of chart we want to create
//     type: 'line',

//     // The data for our dataset
//     data: {
//         labels: ['2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'],
//         datasets: [{
//             label: 'Pagydyti gyvūnai iš viso',
//             backgroundColor: '#80cbc4',
//             borderColor: '#80cbc4',
//             data: [500, 1500, 2800, 4200, 5700, 7800, 9500, 11300]
//         }]
//     },

//     // Configuration options go here
//     options: {}
// });

var ctx2 = document.getElementById('chart2').getContext('2d');
var chart2 = new Chart(ctx2, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: ['Saldainiai', 'Pyktis Kristijonui', 'Kristijono kalbos ne į temą', 'Mokslai'],
        datasets: [{
            backgroundColor: ['#4db6ac', '#ff5252', '#80cbc4', '#4fc3f7'],
            data: [18, 40, 32, 10]
        }]
    },

    // Configuration options go here
    options: {
    	cutoutPercentage: 50,
    }
});

var ctx3 = document.getElementById('chart3').getContext('2d');
var chart3 = new Chart(ctx3, {
    // The type of chart we want to create
    type: 'bar',

    // The data for our dataset
    data: {
        labels: ['04.18', '04.19', '04.20', '04.21', '04.22', '04.23', '04.24', '04.25','04.26'],
        datasets: [{
            label: 'Pyktis procentais',
            backgroundColor: '#00838f',
            borderColor: '#81d4fa',
            data: [0, 98, 64, 10, 86, 24, 74, 42, 5]
        }]
    },

    // Configuration options go here
    options: {}
});

var ctx5 = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx5, {
    type: 'polarArea',
    data: {
        labels: ['Žinių radijas', 'Vaikų dainelės', 'Vaikų pasakos', 'Ieva Narkutė', 'Kita'],
        datasets: [{
            label: 'Veikla',
            data: [12, 19, 10, 7, 2],
            backgroundColor: [                
                'rgba(54, 162, 235, 0.2)',                
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(51, 255, 255, 0.2)',
                'rgba(51, 255, 153, 0.2)'
            ],
            borderColor: [                
                'rgba(54, 162, 235, 1)',                
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(51, 255, 255, 1)',
                'rgba(51, 255, 153, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {}
});

// var ctx4 = document.getElementById('chart4').getContext('2d');
// var chart4 = new Chart(ctx4, {
//     // The type of chart we want to create
//     type: 'radar',

//     // The data for our dataset
//     data: {
//         labels: ['Kruopštumas', 'Atsakingumas', 'Švelnumas', 'Greitis', 'Profesionalumas', 'Žinios'],
//         datasets: [{
//             label: 'Klientų vertinimas',
//             backgroundColor: '#4db6ac',
//             borderColor: '#4db6ac',
//             data: [70, 85, 75, 90, 85, 95]
//         }]
//     },

//     // Configuration options go here
//     options: {}
// });