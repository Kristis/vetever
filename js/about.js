// Initialize and add the map
function initMap() {
  // The location of Vetever
  var vetever = {lat: 54.686141, lng: 25.285979};
  // The map, centered at Vetever
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 16, center: vetever});
  // The marker, positioned at Vetever
  var marker = new google.maps.Marker({position: vetever, map: map});
}